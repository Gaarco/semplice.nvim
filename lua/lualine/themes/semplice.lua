local colors = require("semplice.colors")

return {
    normal = {
        a = { fg = colors.fg, bg = colors.bg_dim, gui = "bold" },
        b = { fg = colors.fg, bg = colors.bg },
        c = { fg = colors.fg, bg = colors.bg },
    },
    insert = {
        a = { fg = colors.fg, bg = colors.bg_dim, gui = "bold" },
        b = { fg = colors.fg, bg = colors.bg },
        c = { fg = colors.fg, bg = colors.bg },
    },
    visual = {
        a = { fg = colors.fg, bg = colors.bg_dim, gui = "bold" },
        b = { fg = colors.fg, bg = colors.bg },
        c = { fg = colors.fg, bg = colors.bg },
    },
    replace = {
        a = { fg = colors.fg, bg = colors.bg_dim, gui = "bold" },
        b = { fg = colors.fg, bg = colors.bg },
        c = { fg = colors.fg, bg = colors.bg },
    },
    command = {
        a = { fg = colors.fg, bg = colors.bg_dim, gui = "bold" },
        b = { fg = colors.fg, bg = colors.bg },
        c = { fg = colors.fg, bg = colors.bg },
    },
    inactive = {
        a = { fg = colors.fg, bg = colors.bg_dim, gui = "bold" },
        b = { fg = colors.fg, bg = colors.bg },
        c = { fg = colors.fg, bg = colors.bg },
    },
}
