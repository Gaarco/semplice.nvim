local M = {}

function M.set(colors)
    return {
        DiagnosticError = { fg = colors.red },
        DiagnosticWarn = { fg = colors.yellow },
        DiagnosticInfo = { fg = colors.fg },
        DiagnosticHint = { fg = colors.blue },
        DiagnosticOk = { fg = colors.green },

        DiagnosticVirtualTextError = { fg = colors.red },
        DiagnosticVirtualTextWarn = { fg = colors.yellow },
        DiagnosticVirtualTextInfo = { fg = colors.fg },
        DiagnosticVirtualTextHint = { fg = colors.blue },
        DiagnosticVirtualTextOk = { fg = colors.green },

        DiagnosticUnderlineError = { fg = colors.red, undercurl = true },
        DiagnosticUnderlineWarn = { fg = colors.yellow, undercurl = true },
        DiagnosticUnderlineInfo = { fg = colors.fg },
        DiagnosticUnderlineHint = { fg = colors.blue },
        DiagnosticUnderlineOk = { fg = colors.green },

        DiagnosticFloatingError = { fg = colors.red },
        DiagnosticFloatingWarn = { fg = colors.yellow },
        DiagnosticFloatingInfo = { fg = colors.fg },
        DiagnosticFloatingHint = { fg = colors.blue },
        DiagnosticFloatingOk = { fg = colors.green },

        DiagnosticSignError = { fg = colors.red },
        DiagnosticSignWarn = { fg = colors.yellow },
        DiagnosticSignInfo = { fg = colors.fg },
        DiagnosticSignHint = { fg = colors.blue },
        DiagnosticSignOk = { fg = colors.green },

        DiagnosticDeprecated = { fg = colors.yellow },
        DiagnosticUnnecessary = { fg = colors.yellow },
    }
end

return M
