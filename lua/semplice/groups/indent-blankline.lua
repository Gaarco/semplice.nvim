local M = {}

function M.set(colors)
    return {
        IblIndent = { fg = colors.bg_dim },
        IblWhitespace = { fg = colors.bg_dim },
        IblScope = { fg = colors.gray1 },
    }
end

return M
