local M = {}

function M.set(colors)
    return {
        Comment = { fg = colors.gray1 },
        Constant = { fg = colors.gray0 },
        String = { fg = colors.orange },
        Character = { fg = colors.orange },
        Number = { fg = colors.gray0 },
        Boolean = { fg = colors.gray0 },
        Float = { fg = colors.gray0 },
        Identifier = { fg = colors.fg },
        Function = { fg = colors.blue },
        Statement = { fg = colors.fg },
        Conditional = { fg = colors.violet },
        Repeat = { fg = colors.violet },
        Label = { fg = colors.orange },
        Operator = { fg = colors.gray0 },
        Keyword = { fg = colors.violet },
        Exception = { fg = colors.orange },
        PreProc = { fg = colors.gray0 },
        Include = { fg = colors.gray0 },
        Define = { fg = colors.gray0 },
        Macro = { fg = colors.gray0 },
        PreCondit = { fg = colors.fg },
        Type = { fg = colors.pink },
        StorageClass = { fg = colors.violet },
        Structure = { fg = colors.pink },
        Typedef = { fg = colors.blue },
        Special = { fg = colors.gray1 },
        SpecialChar = { fg = colors.orange },
        Tag = { fg = colors.orange },
        Delimiter = { fg = colors.gray0 },
        SpecialComment = { fg = colors.gray0 },
        Debug = { fg = colors.yellow },
        Underlined = { fg = colors.pink, underline = true },
        Ignore = { fg = colors.fg },
        Error = { fg = colors.red, undercurl = true },
        Todo = { fg = colors.yellow },
    }
end

return M
