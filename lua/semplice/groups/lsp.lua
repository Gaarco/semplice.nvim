local M = {}

function M.set(colors)
    return {
        LspReferenceText = { fg = colors.blue },
        LspReferenceRead = { fg = colors.blue },
        LspReferenceWrite = { fg = colors.blue },
        LspInlayHint = { fg = colors.gray1 },
        LspCodeLens = { fg = colors.fg },
        LspCodeLensSeparator = { fg = colors.gray1 },
        LspSignatureActiveParameter = { fg = colors.gray1 },

        LspInfoTitle = { fg = colors.blue, bold = true },
        LspInfoList = { fg = colors.blue, bold = true },
        LspInfoFiletype = { fg = colors.blue, bold = true },
        LspInfoTip = { fg = colors.blue, bold = true },
        LspInfoBorder = { fg = colors.gray1 },

        ["@lsp.type.class"] = { fg = colors.pink },
        ["@lsp.type.decorator"] = { fg = colors.orange },
        ["@lsp.type.enum"] = { fg = colors.pink },
        ["@lsp.type.enumMember"] = { fg = colors.fg },
        ["@lsp.type.function"] = { fg = colors.blue },
        ["@lsp.type.interface"] = { fg = colors.pink },
        ["@lsp.type.macro"] = { fg = colors.gray0 },
        ["@lsp.type.method"] = { fg = colors.blue },
        ["@lsp.type.namespace"] = { fg = colors.fg },
        ["@lsp.type.parameter"] = { fg = colors.fg },
        ["@lsp.type.property"] = { fg = colors.fg },
        ["@lsp.type.struct"] = { fg = colors.pink },
        ["@lsp.type.type"] = { fg = colors.pink },
        ["@lsp.type.typeParameter"] = { fg = colors.pink },
        ["@lsp.type.variable"] = { fg = colors.fg },
    }
end

return M
