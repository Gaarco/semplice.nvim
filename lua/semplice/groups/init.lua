local M = {}

M.diagnostics = require("semplice.groups.diagnostics")
M.editor = require("semplice.groups.editor")
M.gitsigns = require("semplice.groups.gitsigns")
M.indent_blankline = require("semplice.groups.indent-blankline")
M.lazy = require("semplice.groups.lazy")
M.lsp = require("semplice.groups.lsp")
M.nvim_cmp = require("semplice.groups.nvim-cmp")
M.oil = require("semplice.groups.oil")
M.pounce = require("semplice.groups.pounce")
M.syntax = require("semplice.groups.syntax")
M.telescope = require("semplice.groups.telescope")
M.treesitter = require("semplice.groups.treesitter")
M.window_picker = require("semplice.groups.window-picker")

return M
