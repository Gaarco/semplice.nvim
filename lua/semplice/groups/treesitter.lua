local M = {}

function M.set(colors)
    return {
        ["@variable"] = { fg = colors.fg },
        ["@variable.builtin"] = { fg = colors.fg },
        ["@variable.parameter"] = { fg = colors.gray0 },
        ["@variable.parameter.builtin"] = { fg = colors.gray0 },
        ["@variable.member"] = { fg = colors.fg },

        ["@constant"] = { fg = colors.gray0 },
        ["@constant.builtin"] = { fg = colors.gray0 },
        ["@constant.macro"] = { fg = colors.gray0 },

        ["module"] = { fg = colors.fg },
        ["module.builtin"] = { fg = colors.fg },
        ["@label"] = { fg = colors.orange },

        ["@string"] = { fg = colors.orange },
        ["@string.documentation"] = { fg = colors.gray0 },
        ["@string.regex"] = { fg = colors.orange },
        ["@string.escape"] = { fg = colors.gray1 },
        ["@string.special"] = { fg = colors.orange },
        ["@string.special.symbol"] = { fg = colors.orange },
        ["@string.special.url"] = { fg = colors.blue, underline = true },
        ["@string.special.path"] = { fg = colors.blue, underline = true },

        ["@character"] = { fg = colors.orange },
        ["@character.special"] = { fg = colors.orange },

        ["@boolean"] = { fg = colors.gray0 },
        ["@number"] = { fg = colors.gray0 },
        ["@number.float"] = { fg = colors.gray0 },

        ["@type"] = { fg = colors.pink },
        ["@type.builtin"] = { fg = colors.pink },
        ["@type.definition"] = { fg = colors.pink },

        ["@attribute"] = { fg = colors.blue },
        ["@attribute.builtin"] = { fg = colors.blue },
        ["@property"] = { fg = colors.fg },

        ["@function"] = { fg = colors.blue },
        ["@function.builtin"] = { fg = colors.blue },
        ["@function.call"] = { fg = colors.blue },
        ["@function.macro"] = { fg = colors.gray0 },

        ["@function.method"] = { fg = colors.blue },
        ["@function.method.call"] = { fg = colors.blue },

        ["@constructor"] = { fg = colors.blue },
        ["@operator"] = { fg = colors.gray0 },

        ["@keyword"] = { fg = colors.violet },
        ["@keyword.coroutine"] = { fg = colors.violet },
        ["@keyword.function"] = { fg = colors.violet },
        ["@keyword.operator"] = { fg = colors.gray0 },
        ["@keyword.import"] = { fg = colors.gray0 },
        ["@keyword.type"] = { fg = colors.gray0 },
        ["@keyword.modifier"] = { fg = colors.violet },
        ["@keyword.repeat"] = { fg = colors.violet },
        ["@keyword.return"] = { fg = colors.violet },
        ["@keyword.debug"] = { fg = colors.yellow },
        ["@keyword.exception"] = { fg = colors.red },

        ["@keyword.conditional"] = { fg = colors.violet },
        ["@keyword.conditional.ternary"] = { fg = colors.gray0 },

        ["@keyword.directive"] = { fg = colors.gray0 },
        ["@keyword.directive.define"] = { fg = colors.gray0 },

        ["@punctuation.delimiter"] = { fg = colors.gray0 },
        ["@punctuation.bracket"] = { fg = colors.fg },
        ["@punctuation.special"] = { fg = colors.fg },

        ["@comment"] = { fg = colors.gray1 },
        ["@comment.documentation"] = { fg = colors.gray0 },

        ["@comment.error"] = { fg = colors.red },
        ["@comment.warning"] = { fg = colors.yellow },
        ["@comment.todo"] = { fg = colors.yellow },
        ["@comment.note"] = { fg = colors.blue },

        ["@markup.strong"] = { fg = colors.fg, bold = true },
        ["@markup.italic"] = { fg = colors.fg, italic = true },
        ["@markup.strikethrough"] = { fg = colors.fg, strikethrough = true },
        ["@markup.underline"] = { fg = colors.fg, underline = true },

        ["@markup.heading"] = { fg = colors.blue, bold = true, italic = true },
        ["@markup.heading.1"] = { fg = colors.blue, bold = true, italic = true },
        ["@markup.heading.2"] = { fg = colors.blue, italic = true },
        ["@markup.heading.3"] = { fg = colors.blue, italic = true },
        ["@markup.heading.4"] = { fg = colors.blue, italic = true },
        ["@markup.heading.5"] = { fg = colors.blue, italic = true },
        ["@markup.heading.6"] = { fg = colors.blue, italic = true },

        ["@markup.quote"] = { fg = colors.gray0, italic = true },
        ["@markup.math"] = { fg = colors.gray0 },

        ["@markup.link"] = { fg = colors.blue, underline = true },
        ["@markup.link.label"] = { fg = colors.blue, underline = true },
        ["@markup.link.url"] = { fg = colors.blue, underline = true },

        ["@markup.raw"] = { fg = colors.fg },
        ["@markup.raw.block"] = { fg = colors.fg },

        ["@markup.list"] = { fg = colors.gray0 },
        ["@markup.list.checked"] = { fg = colors.gray0 },
        ["@markup.list.unchecked"] = { fg = colors.gray0 },

        ["@diff.plus"] = { fg = colors.green },
        ["@diff.minus"] = { fg = colors.red },
        ["@diff.delta"] = { fg = colors.yellow },

        ["@tag"] = { fg = colors.fg },
        ["@tag.builtin"] = { fg = colors.fg },
        ["@tag.attribute"] = { fg = colors.orange },
        ["@tag.delimiter"] = { fg = colors.gray0 },

        ["@none"] = { fg = colors.fg, bg = colors.bg },
        ["@conceal"] = { fg = colors.gray1 },

        ["@spell"] = {},
        ["@nospell"] = {},
    }
end

return M
