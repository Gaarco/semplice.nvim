local M = {}

function M.set(colors)
    return {
        LazyButton = { fg = colors.fg },
        LazyButtonActive = { fg = colors.fg, bg = colors.bg_dim },
        LazyComment = { fg = colors.gray1 },
        LazyCommit = { fg = colors.pink },
        LazyCommitIssue = { fg = colors.gray0 },
        LazyCommitScope = { fg = colors.gray0 },
        LazyCommitType = { fg = colors.fg },
        LazyDimmed = { fg = colors.gray1 },
        LazyDir = { fg = colors.blue },
        LazyH1 = { fg = colors.fg, bold = true },
        LazyH2 = { fg = colors.gray0, bold = true },
        LazyLocal = { fg = colors.orange },
        LazyNoCond = { fg = colors.orange },
        LazyNormal = { fg = colors.fg, bg = colors.bg },
        LazyProgressDone = { fg = colors.green },
        LazyProgressTodo = { fg = colors.yellow },
        LazyProp = { fg = colors.gray0 },
        LazyReasonCmd = { fg = colors.violet },
        LazyReasonEvent = { fg = colors.violet },
        LazyReasonFt = { fg = colors.violet },
        LazyReasonImport = { fg = colors.violet },
        LazyReasonKeys = { fg = colors.violet },
        LazyReasonPlugin = { fg = colors.violet },
        LazyReasonRequire = { fg = colors.violet },
        LazyReasonRuntime = { fg = colors.violet },
        LazyReasonSource = { fg = colors.violet },
        LazyReasonStart = { fg = colors.violet },
        LazySpecial = { fg = colors.orange, bold = true, italic = true },
        LazyTaskError = { fg = colors.red },
        LazyTaskOutput = { fg = colors.orange },
        LazyUrl = { fg = colors.blue, underline = true },
        LazyValue = { fg = colors.orange },
    }
end

return M
