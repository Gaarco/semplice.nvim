local M = {}

function M.set(colors)
    return {
        PounceMatch = { fg = colors.blue, underline = true },
        PounceUnmatched = { fg = colors.gray0 },
        PounceGap = { fg = colors.gray1 },
        PounceAccept = { fg = colors.blue, underline = true, bold = true },
        PounceAcceptBest = { fg = colors.bg, bg = colors.blue, bold = true },
    }
end

return M
