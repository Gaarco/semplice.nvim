local M = {}

function M.set(colors)
    return {
        WindowPicker = { fg = colors.bg, bg = colors.blue, bold = true },
        WindowPickerSwap = { fg = colors.bg, bg = colors.violet, bold = true },
        WindowPickerZap = { fg = colors.bg, bg = colors.pink, bold = true },
    }
end

return M
