local M = {}

function M.set(colors)
    return {
        TelescopeSelection = { fg = colors.fg, bg = colors.bg },
        TelescopeSelectionCaret = { fg = colors.fg, bg = colors.bg },
        TelescopeMultiSelection = { fg = colors.fg, bg = colors.bg, bold = true },
        TelescopeNormal = { fg = colors.fg, bg = colors.bg },
        TelescopeBorder = { fg = colors.gray1, bg = colors.bg },
        TelescopeMatching = { fg = colors.blue, underline = true },
        TelescopePromptPrefix = { fg = colors.fg },
    }
end

return M
