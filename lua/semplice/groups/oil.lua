local M = {}

function M.set(colors)
    return {
        OilDir = { fg = colors.blue },
        OilDirIcon = { fg = colors.blue },
        OilSocket = { fg = colors.gray0 },
        OilLink = { fg = colors.gray0 },
        OilLinkTarget = { fg = colors.fg },
        OilFile = { fg = colors.fg },
        OilCreate = { fg = colors.green },
        OilDelete = { fg = colors.red },
        OilMove = { fg = colors.fg },
        OilCopy = { fg = colors.fg },
        OilChange = { fg = colors.green },
        OilRestore = { fg = colors.green },
        OilPurge = { fg = colors.red },
        OilTrash = { fg = colors.red },
        OilTrashSourcePath = { fg = colors.blue, underline = true },
    }
end

return M
