local M = {}

function M.set(colors)
    return {
        GitSignsAdd = { fg = colors.green },
        GitSignsChange = { fg = colors.yellow },
        GitSignsDelete = { fg = colors.red },
        GitSignsChangedelete = { fg = colors.red },
        GitSignsTopdelete = { fg = colors.red },
        GitSignsUntracked = { fg = colors.blue },
        GitSignsAddNr = { fg = colors.green },
        GitSignsChangeNr = { fg = colors.yellow },
        GitSignsDeleteNr = { fg = colors.red },
        GitSignsChangedeleteNr = { fg = colors.red },
        GitSignsTopdeleteNr = { fg = colors.red },
        GitSignsUntrackedNr = { fg = colors.blue },
        GitSignsAddLn = { fg = colors.bg, bg = colors.green },
        GitSignsChangeLn = { fg = colors.bg, bg = colors.yellow },
        GitSignsDeleteLn = { fg = colors.bg, bg = colors.red },
        GitSignsChangedeleteLn = { fg = colors.bg, bg = colors.red },
        GitSignsUntrackedLn = { fg = colors.bg, bg = colors.blue },
        GitSignsAddPreview = { fg = colors.green },
        GitSignsDeletePreview = { fg = colors.red },
        GitSignsCurrentLineBlame = { fg = colors.gray0 },
        GitSignsAddInline = { fg = colors.green },
        GitSignsDeleteInline = { fg = colors.red },
        GitSignsChangeInline = { fg = colors.yellow },
        GitSignsAddLnInline = { fg = colors.green },
        GitSignsChangeLnInline = { fg = colors.yellow },
        GitSignsDeleteLnInline = { fg = colors.red },
        GitSignsDeleteVirtLn = { fg = colors.red },
        GitSignsDeleteVirtLnInLine = { fg = colors.red },
        GitSignsVirtLnum = { fg = colors.fg },
    }
end

return M
