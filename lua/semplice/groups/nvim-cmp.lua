local M = {}

function M.set(colors)
    return {
        CmpItemAbbr = { fg = colors.fg },
        CmpItemAbbrDeprecated = { fg = colors.gray1, strikethrough = true },
        CmpItemAbbrMatch = { fg = colors.orange },
        CmpItemAbbrMatchFuzzy = { fg = colors.orange },
        CmpItemMenu = { fg = colors.gray1 },

        CmpItemKindClass = { fg = colors.pink },
        CmpItemKindColor = { fg = colors.red },
        CmpItemKindConstant = { fg = colors.fg },
        CmpItemKindConstructor = { fg = colors.blue },
        CmpItemKindEnum = { fg = colors.pink },
        CmpItemKindEvent = { fg = colors.violet },
        CmpItemKindField = { fg = colors.fg },
        CmpItemKindFile = { fg = colors.blue },
        CmpItemKindFolder = { fg = colors.blue },
        CmpItemKindFunction = { fg = colors.blue },
        CmpItemKindInterface = { fg = colors.pink },
        CmpItemKindKeyword = { fg = colors.violet },
        CmpItemKindMember = { fg = colors.fg },
        CmpItemKindMethod = { fg = colors.blue },
        CmpItemKindModule = { fg = colors.fg },
        CmpItemKindOperator = { fg = colors.gray0 },
        CmpItemKindProperty = { fg = colors.fg },
        CmpItemKindReference = { fg = colors.fg },
        CmpItemKindSnippet = { fg = colors.fg },
        CmpItemKindStruct = { fg = colors.pink },
        CmpItemKindText = { fg = colors.fg },
        CmpItemKindTypeParameter = { fg = colors.fg },
        CmpItemKindUnit = { fg = colors.pink },
        CmpItemKindValue = { fg = colors.fg },
        CmpItemKindVariable = { fg = colors.fg },
    }
end

return M
