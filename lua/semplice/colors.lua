-- We try to have all colors passing AAA test
-- where it is not possible we fall back to AA test
-- https://accessible-colors.com/

local M = {}

M.dark = {
    none = "NONE",

    bg = "#101010",
    bg_dim = "#1A1A1A",
    fg = "#E6E6E6",

    gray0 = "#808080",
    gray1 = "#595959",

    blue = "#9EA6FA",
    violet = "#C579EC",
    pink = "#F5A3A3",
    orange = "#F4A871",

    green = "#8DE269",
    red = "#E44F44",
    yellow = "#EDE35E",
}

M.light = {
    none = "NONE",

    bg = "#101010",
    bg_dim = "#1A1A1A",
    fg = "#E6E6E6",

    gray0 = "#808080",
    gray1 = "#595959",

    blue = "#9EA6FA",
    violet = "#C579EC",
    pink = "#F5A3A3",
    orange = "#F4A871",

    green = "#8DE269",
    red = "#E44F44",
    yellow = "#EDE35E",
}

return M
