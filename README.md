<h1 align="center">Semplice</h1>
<p align="center">
    A dark colorscheme that aims to use few distinctive colors on a non-blue background
</p>

## Setup

The only required step is to install the colorscheme using your plugin manager of choice.\
If necessary, execute the following:
```lua
vim.cmd.colorscheme("semplice")
```

## Why there's no customization

The common configuration pattern that makes use of a setup function with a Lua table would allow
customizing only a subset of the settings while gradually making the plugin more complex.\
I appreciate simplicity where appropriate. In my opinion, a colorscheme should only provide the
colors, and if necessary, users can customize everything they want by manually overwriting the
highlights.

## Plugins supported out-of-the-box

- [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim)
- [indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim)
- [lazy.nvim](https://github.com/folke/lazy.nvim)
- [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim)
- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
- [oil.nvim](https://github.com/stevearc/oil.nvim)
- [pounce.nvim](https://github.com/rlane/pounce.nvim)
- [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim)
- [window-picker.nvim](https://github.com/ten3roberts/window-picker.nvim)

These are basically the ones I use, if you want any other plugin to be supported open an issue or
create a pull request.
