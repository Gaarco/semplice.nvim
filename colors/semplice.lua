local M = {}

M.setup = function(variant)
    for k in pairs(package.loaded) do
        if k:match(".*semplice.*") then
            package.loaded[k] = nil
        end
    end

    vim.cmd("hi clear")
    if vim.fn.exists("syntax_on") then
        vim.cmd("syntax reset")
    end

    vim.g.termguicolors = true
    vim.g.colors_name = "semplice"

    local colors = require("semplice.colors")
    local groups = require("semplice.groups")

    for _, hl_group in pairs(groups) do
        local hl = hl_group.set(colors[variant or vim.o.background])
        for group, properties in pairs(hl) do
            vim.api.nvim_set_hl(0, group, properties)
        end
    end
end

M.setup()

return M
